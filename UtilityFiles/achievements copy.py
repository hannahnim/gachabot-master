# Utility file which holds external classes and functions, planning on seperating this to seperate files later
import discord
import sys
from discord.ext import menus
from typing import List, Dict
from openpyxl import load_workbook
from dataclasses import dataclass



sys.path.append("..")  # allows UtilityFiles to import
from UtilityFiles.constants import COLOR, RARITY_MAP, DAILY_TIME, PREFIX, GRADES, \
    RARITY_VALUE  # imports settings

# imports utility functions, will be split up later
from UtilityFiles.util import get_inventory, exel_reader, get_gachas, give_card, add_card, get_pool, dump_data, \
    is_allowed, NoGachas, ShopConfirmMenu, TradeConfirmMenu, \
    GachaCard, CardMenu, CardMenuSource, ShowGachaMenu, ConvertGachaMenu, get_score, LotteryGachaMenu

import json

#  Temporarily putting this global here, for Achievements.
GACHA_DATA = exel_reader("UtilityFiles/GACHAUNITS.xlsx")

with open("UtilityFiles/achievements.json", "r") as f:
    achievements = json.load(f)  # list of id #'s to make a pool

async def add_achievement_card(ctx, user, achievement_id:str):
    achievement_name = achievements[achievement_id]["name"]
    hidden = achievements[achievement_id]["hidden"]
    card_id = achievements[achievement_id]["character"]
    ## Ternary operator to check if its hidden. If yes just adds "hidden" to the string
    server_message = discord.Embed(title=f'{user.name} has earned the {"hidden " if hidden else ""}{achievement_name} achievement!', colour=COLOR)
    await ctx.send(embed=server_message)
    dm_message = discord.Embed(title=f'You earned the {"hidden " if hidden else ""}{achievement_name} achievement!', colour=COLOR)
    await user.send(embed=dm_message)
    await give_card(ctx, user, card_id, True)
    await achievement_checker(ctx, ctx.author, "unitadd", card_id)  # Checks achievements

async def achievement_checker(ctx, user, ach_info, value):
    # This is where the main checks for achievements will happen. Will be called a lot.
    # Probably a good idea to separate sections of code on categories, for organization and functionality's sake.
    # This is going to suck, no way to avoid it. Separate into its own file ASAP? Organization's sake.

    # <editor-fold desc="Idea... Proposal? Possibility:">
    # Coding achievements in a traditional videogame sense:
    #
    # Instead of making the code check if a certain quota is met using the user's global statistics, i.e.
    # ###----------------------Assuming we work based on the user's total gachas-----------------###
    #   for unit in usergachas:  (Get total Gachas)
    #       total_unit_amount += int(unit[1])
    #       (Actual absolute literal shit ton of checks to see 'if' there's X amount of characters from X series)
    #   (More 'if' checks for stuff like Hoarder)
    # #--------------------------------------------------------------------------------------------#
    # We could instead start adding on to a user-specific achievement-independent tracking.
    # For instance, in the current example of "UNIT" achievements:
    #
    # Let's assume the character rolled was Goku. Getting him would give the user progress towards Hoarder, 90's kid
    # and possibly some other achievements. We'd make that check:
    # ###----------------------Assuming we work based on the user's latest-rolled gacha----------###
    #   (lot of "if" checks to see if he's from any specific series, for stuff like the "Magical Girl" achievement.)
    #
    #    #  90's Kid
    #   if (ach_value.show == "Dragon Ball Z" | "Yu Yu Hakusho" | "Sailor Moon" | "Cardcaptor Sakura" |
    #   "Pokemon" | "Revolutionary Girl Utena" | "Neon Genesis Evangelion" | "Cowboy Bebop"):
    #       user_inventory["achievements"]["10"] += 1
    # #--------------------------------------------------------------------------------------------#
    # Assuming Goku is their only owned Gacha, their entry on user_info.json would look a little bit like:
    #   "achievements": {
    #   "1":1,
    #   "10":1
    #   }
    # How much you need for you do actually "Have" each achievement would be on achievement.json
    # Note: Said values added to achievements.json. "goals" with a value of 1 should be treated as bools, either 0 or 1.
    # I'm pretty sure making them actual bools would conflict with user_info.json
    #
    # Not only would this be a bit more optimal, since we don't have to do any unnecessary checks in a "for" loop,
    # but this would also let us tell the user how far into any non-secret achievement they are.
    # We can check if they've met the quota for any unowned achievement at the end of the code.
    #
    # The only clear downsides are:
    # -This needs a one-time "catch-up" code, so we can update all the current users in
    # user_info with the right achievement stats for what they currently have. Otherwise, a person with 500 gachas
    # might have a progress of 3/250 in "Hoarder"
    # -Necessary to keep in mind the removal of units via trades & conversions.
    # Keep in mind, if someone has 5 Narutos, and converts or trades one away, they should still keep their achievement.
    #
    #
    # After heavy consideration, this is... Mostly good, I think? It has a small few flaws. Worth doing it and
    # going around the flaws, handling the specific achievements differently. Current found flaws:
    #
    # -The achievements:
    # "I'll Be Here All Week", "You're Going to Be a Star!", "This is How I Win" & "Frugal", "Show-off"
    # don't have 'additive' goals by nature.
    # Frugal needs you to own 100 coins at once, for instance. Can be handled separately, no problem.
    # Get them with specific "ach_info" values called wherever necessary, i.e. the !daily command.
    #
    # -"Fan" & "Big Fan needs us to check all gachas anyway.
    # Is this still more optimal even with them included? Should be.
    # Should check if user has these achievements before doing their checks.
    #
    # -"Zoologist" could possibly need a check of all the user owned gachas. This is probably fine, as we can just do
    # the check when the drawn unit is a cat, dog or any other animal.
    # The same goes for the achievements "Chun III", "New Horizons", "Gentleman Thief", "Nice." & "NICE."
    # </editor-fold>

    # Temp.TODO:Move this to somewhere permanent like constants.py before pushing, we don't need to call this everytime.


    user_inventory = get_inventory(str(user.id))
    user_achievements = user_inventory["achievements"]
    # Note: We always check if the user has the achievement before the achievement's actual checks for readability
    # This isn't always the best, however. Optimization-wise.
    # Consider making the achievement's check first for the harder achievements.
    # i.e. Cat person, Does Not Compute, New Horizons
    # "UNITADD" achievements. Achievements that are called if a unit is added to the user's inventory. Roll & Store.
    # Currently all unit-related achievements are here. That needs change.
    if ach_info == "unitadd":

        # 1: Hoarder & 56: Weeb
        ## Keeps Track even after they're achieved for future proofing purposes
        user_achievements["56"] += 1
        user_achievements["1"] += 1
        ## Saves data after achievement is gotten. Probably not the best way to do this but these would only be called once per user so not a priority
        ## If this isn't done it results in an infinite loop or in rare cases to the user not getting an achievement
        if user_achievements["56"] == 500:
            with open("UtilityFiles/user_info.json", "r+") as out:
                stored_info = json.load(out)
                stored_info[str(user.id)]["achievements"] = user_achievements
                dump_data(data=stored_info, file=out)
        if user_achievements["1"] == 250:
            with open("UtilityFiles/user_info.json", "r+") as out:
                stored_info = json.load(out)
                stored_info[str(user.id)]["achievements"] = user_achievements
                dump_data(data=stored_info, file=out) 


        # 2: Fan & 31 : Big Fan. (Gross, fix this, Uri. And make it not shit. And faster.)
        ## Made it slightly less gross but I don't know how to optimise this further
        if user_achievements["31"] == 0:
            showdict = {}
            usergachas = get_gachas(user, excel_data=GACHA_DATA)
            if user_achievements["2"] == 0:
                for gacha in usergachas:
                    if gacha.show in showdict.keys():  # I'd rather not have to do this check. Possible solutions?
                        showdict[gacha.show] += gacha.amount
                        if showdict[gacha.show] >= 5:
                            user_achievements["2"] = 1
                            
                    else:
                        showdict[gacha.show] = gacha.amount
            # Gross. Same loop, except if we need "Big Fan" and not "Fan", we check for 10 instead.
            else:  # Is the small optimization worth the grossness?
                for gacha in usergachas:
                    if gacha.show in showdict.keys():
                        showdict[gacha.show] += gacha.amount
                        if showdict[gacha.show] >= 25:
                            user_achievements["31"] = 1
                            
                    else:
                        showdict[gacha.show] = gacha.amount

        # 3: Collector & 32: Connossieur
        print(value.rarity)
        if value.rarity == "UR":
            if user_achievements["32"] < 25:
                user_achievements["32"] += 1
                
                if user_achievements["3"] < 10:
                    user_achievements["3"] += 1
                    

        # 10: 90s Kid
        if user_achievements["10"] < 25:
            if (value.show in ("Dragon Ball Z", "Yu Yu Hakusho", "Sailor Moon", "Cardcaptor Sakura",
               "Pokemon", "Revolutionary Girl Utena", "Neon Genesis Evangelion", "Cowboy Bebop")):
                user_achievements["10"] += 1
                

        # 11: Magical Girl
        if user_achievements["11"] < 15:
            if value.show in ("Sailor Moon", "Cardcaptor Sakura", "Revolutionary Girl Utena"):
                user_achievements["11"] += 1
                

        # 12: Fujoshi
        if user_achievements["12"] < 15:
            if value.show in ("Yuri!!! On Ice", "Haikyuu", "Banana Fish", "Fruits Basket"):
                user_achievements["12"] +=1
                

        # 13: Your 30-something Older Brother
        if user_achievements["13"] < 15:
            if value.show in ("Cowboy Bebop", "Berserk", "Rurouni Kenshin", "Akira"):
                user_achievements["13"] += 1
                

        # 14: Shonen
        if user_achievements["14"] < 25:
            if value.show in ("Naruto", "One Piece", "Hunter x Hunter", "Bleach", "Demon Slayer", "My Hero Academia"):
                user_achievements["14"] += 1
                

        # 17: Believe It!
        if user_achievements["17"] < 5:
            if value.card_id == 1:
                user_achievements["17"] += 1
                

        # 19: Gamer
        if user_achievements["19"] < 30:
            if value.header == "Video Game":
                user_achievements["19"] += 1
                

        # 20: Zoologist. This is readable, but it sucks. Only a couple of rarer units trigger this, though, so it's fine
        if user_achievements["20"] == 0:
            if value.descriptor in ("Dog", "Cat", "Animal"):
                usergachas = get_gachas(user, excel_data=GACHA_DATA)
                cat = False
                dog = False
                animal = False
                for gacha in usergachas:
                    if gacha.descriptor == cat:
                        cat = True
                    elif gacha.descriptor == dog:
                        dog = True
                    elif gacha.descriptor == animal:
                        animal = True
                if cat and dog and animal:
                    user_achievements["20"] += 1
                    

        # 28: Nice.
        if user_achievements["28"] == 0:
            if value.card_id == 69 | 420:
                if set({'69', '420'}).issubset(user_inventory["gachas"].keys()):
                    user_achievements["28"] = 1
                    

        # 39: Ghibli Fanboy
        if user_achievements["39"] < 30:
            if value.ghibli is not None:
                user_achievements["39"] += 1
                

        # 41: Chun III
        if user_achievements["41"] == 0:
            if value.card_id == 464 | 465 | 477:
                if set({'464', '465', '477'}).issubset(user_inventory["gachas"].keys()):
                    user_achievements["41"] = 1
                    

        # 42: Cat Person
        if user_achievements["42"] < 5:
            if value.descriptor == "Cat":
                user_achievements["42"] += 1
                

        # 43: Does Not Compute
        if user_achievements["43"] < 5:
            if value.descriptor == "Robot":
                user_achievements["43"] += 1
                

        # 44: New Horizons
        if user_achievements["44"] == 0:
            if 491 <= value.card_id <= 500:
                if set({range(491, 500)}).issubset(user_inventory["gachas"].keys()):
                    user_achievements["44"] = 1
                    

        # 47: Pokemon Trainer
        if user_achievements["47"] < 6:
            if value.descriptor == "Pokemon":
                user_achievements["47"] += 1
                

        # 49: Monopoly. Too tired to do this one well at the moment.
        if user_achievements["49"] < 5:
            if str(value.card_id) in user_inventory["gachas"].keys():
                user_achievements["49"] += 1

        # 51: NICE.
        if user_achievements["51"] == 0:
            if value.card_id == 69 | 169 | 269 | 369 | 469:
                if set({'69', '169', '269', '369', '469'}).issubset(user_inventory["gachas"].keys()):
                    user_achievements["28"] = 1

        # 64: Gentleman Thief. The achievement says "Own all 4 Lupin units of a different color suit"
        # However, my gacha sheet only has 3 Lupins. Verify before coding.

        ##Theres probably a better way to do this than to write the entire achievements list back to the json but it will suffice
        with open("UtilityFiles/user_info.json", "r+") as out:
            stored_info = json.load(out)
            stored_info[str(user.id)]["achievements"] = user_achievements
            dump_data(data=stored_info, file=out)
        
        await give_out_cards(ctx, user, user_achievements)

    return

async def give_out_cards(ctx, user, user_achievements):
    i = 1
    for achievement in user_achievements.values():
        if achievement is achievements[str(i)]["goal"]:
            await add_achievement_card(ctx, user, str(i))
            user_achievements[str(i)] = 9999
            with open("UtilityFiles/user_info.json", "r+") as out:
                stored_info = json.load(out)
                stored_info[str(user.id)]["achievements"] = user_achievements
                dump_data(data=stored_info, file=out)
        i += 1