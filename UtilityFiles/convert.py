def convert():
    re_name = r".+:"
    re_desc = r":.+\({1}"
    re_card = r"\(.+\)"
    hidden = False
    names = []
    descs = []
    cards = []
    with open("text.txt", r) as f:
        lines = f.readlines()
        for line in lines:
            name = re.search(re_name, line).group(1).replace(":", "")
            desc = re.search(re_desc, line).group(1).replace(": ", "").replace(" (", "")
            card = re.search(re_card, line).group(1)
            out = '''  "1": {
                        "name": "{name}",
                        "desc": "Own 250 Units",
                        "character": "554",
                        "hidden": false,
                        "goal": 250,
                        "level": 1
                        },'''
