GACHA BOT:

According to the previous dev 40% of the Achievements have been completed

Functionality Updates:

Achievement system DONE
Confirm !AddAll commands function properly DONE
Commands like !inventory/!give/!addticket also work with DiscordID DONE
Trade offers to last for 24hrs before expiring
Ability to dedicate Special Pool presets (presubmably on the excel sheet). eg !setspecialpool videogames
(Optional) Polls. There's probably already a bot that does this. Maybe you know of one, or we can incorporate one
(Optional) Quizzes. Similar to polls. I'd like the ability to create quizzes for the server. Maybe with customizable rewards
loop check command. To see if loop is currently in effect (for login/baseball/lottery loops)
Fix store functionality (randomize function that randomly assigns a one unit of each rarity to the store + 2 SALE items and keeps static store items)
User Stats command. (DMs you stats of a user) TODO
	Rolls
	Total Units Owned
	Current Login Streak
	Conversions
	Store Purchases
	Total Coins spent in store
	Trades
	Achievements Earned
	Bets Made
	Total coins lost in bets
	Total coins won in bets
	Baseball Team Name
	Baseball wins
	Baseball losses
	Baseball championships won
	Baseball 2nd Place finishes
	War Game wins
	War Game losses
	War Game wins as champ
	Chess wins
	Chess losses

Emoji Flair for current Leaderboard #1 (STAR), Baseball Champ (BASEBALL), and War Game Belt Holder (CROWN) TODO
Updated !help menu (that DMs the user). see below
Method for logging chess wins (separate bot). probably just an admin command !chessresult @userwinner @userloser
Command to show list of collection (maybe DM'ed to user).  something where all units + stats are listed without pictures
	would like list to be sortable by each stat and total of stats, in addition to name, rarity, count, show
	(Optional) would be very nice if you could create specific lists. !list naruto and shows all the naruto characters you own. Or !list UR and shows all the UR units you own. !list height 60 would show only units with height 60 or above

BASEBALL GAME:

Random AI teams to fill out the league (I'd like the ability to update the AI teams and maybe add multipliers to them) 
16 teams. 2 Daily games
Track team's records/Winners of previous seasons.

Ability to turn season off/on

Seasons are 14 days long 
2 day of team setting (pre-season)
8 days of games. 2 games happen automatically a day at 2pm and 8pm EST
1 day announcement of Top 4 tournament + betting on semifinals
1 day of Semi Finals (both matches at 4pm)
1 day betting on Final
1 Day finals match (4pm)p

baseball loop check command, to see where we are in the season
Ability to start/stop season


!setTeam command
confirmation command showing positions
!ViewTeam command. 
Command to see current team composition of any user (including AI Teams), including the stats being counted and totals for Batting/fielding power and any multipliers

Ability to set team name. This would be done in preseason, or else it will default to @Username's Team.
Once season starts, name should be locked in.

Would also like a command to randomize your team from units your own

If you don't set your team before a season begin, the bot should randomize your team for you

!Standings command. would show each teams records so far this season, sorted by best to worst

Ability to bet on Semi Finals and Finals matches using coins.

Announcements of daily results would be automatic. Would also display current leaders (potentially optional message if leader has changed)

The announcement of the Top 4 would be automatic, and the schedule of the first round of games would be announced shortly after.
After that, users will be able to bet, up until right before the games take place. (We can discuss how complex betting can be)

Players get a coin for each win.

World Series Winner gets flair for next two weeks. 
Winner gets 10 tickets, 3 Special Tickets, 2 Ultra Tickets and 10 coins. 
2nd place gets 4 tickets, 2 special tickets, 2 Super Tickets and 5 coins.
the other two top 4 finishers get 3 tickets and 3 coins.
(obviously the AI teams don't actually get tickets)

Set Units to team: Pitcher, Catcher, 1st Base, 2nd Base, Shortstop, 3rd Base
3 outfields, COACH

Stats: Height, Intelligence, Speed, Fortitude, Power

Pitcher: Height, Speed, Power
Catcher: Intelligence, Fortitude
1st Base: Height, Fortitude
2nd Base: Speed, Intelligence
Shortstop: Speed, Fortitude
3rd Base: Height, Intelligence
Outfields: N/A
Coach: Intelligence, Intelligence

Stats of above positions determine Fielding Power

Power of each Unit (except coach) added up equals Batting Power

Combine these and multiply by random factor (1.05-1.2) and then any other applied multipliers (From Achievement currently set and anything else TBD).

(Fielding Power + Batting Power)*(Random Factor+other factors)= TOTAL

Potential messages accompanying result depending on difference in teams stats and result of random factor

Normal win: Team A defeats Team B.
Difference of 300 or more. Team A CRUSHES Team B. 
Difference of 50 or less. Team A narrowly edges out Team B.
Random factor of 1.05-1.08 (selects one of 5 possible messages at random)
	- After Team B forgets their bats at home
	- After Pitcher _____ breaks both arms mid-game
	- Team B reportedly sick after eating a bad batch of oysters last night
	- Locker room interview reveals Shortstop ______ of Team B cursed by powerful demon
	- Coach ____ claims Team A was cheating
Random factor of 1.17-1.2  (selects one of 5 possible messages at random)
	- Team A receives rulers blessing.
	- Outfielder ____ of Team A goes super saiyan at the plate.
	- 1st Baseman _____ reportedly trained under a waterfall for 3 months prior to game
	- Unconfirmed claims of Catcher ____ opening Maw of Hell in opposing Team B's dugout
	- Plot Armor proves Team A to be protagonist of this season.

For example: Tony's Knights defeats The Coolguys. After Pitcher Naruto breaks both arms mid-game

AI Team Names (Randomized each season). Fills in remaining of the 16 slots. Each team is composed of a random set of units

Mabase Martians
Philly Pies
Busan Lotte Giants
Team Rocket
Chuo Worms
Hanwa Eagles
The Akatsuki
Ginyu Force
Karasuno High
The Pears
The Facebook Boomers
Team Tangie
New York Millenials
The Expatriates
Juneau Doggies
Detroit (Become) Humans 
Harrisburg Senators
Boston Red Scare
Little Sluggers
Cubbie Bears
The Foot Beans
Baublitz Bombers
Mitch Broke Suff's TV
New Cumberland Landlords
The Cow Tools
The Griefers


Championship history to be recorded somehow. Previous season's winners/runner-ups memorialized:

Season 1- Champion: The Coders (@Uri) Runner-Up: The Pears (AI)
Season 2- Champion: Ginyu Force (AI) Runner-Up: API Tools (@Uri)
etc...

This would show Team Names and Username or AI. (since users can change their team name). 

OPTIONAL FUNCTIONALITY- apply additional multipliers for team synergy. eg #of players from the same show
		      - Previous series winner cannot use same 10 players next season


WAR (CARD GAME):

!WarChallenge UserName #ofCoinsforBet 
other user reacts to accept. (expires in 24 hours)

Bot Chooses 5 cards at random to make a deck (facedown)
Each Player Draws and compares TOTAL stats. Each draw would be a command/reaction from the two players that reveal each players card (unit) and displays total power.
Player with 3 or more wins is the victor. Victor gets betted coins. (Failure to react within 24 hours results in a loss of that draw).
Games could last several days due to time zone differences

Random User starts with crown. If user loses, the person who beats them gets the crown. 
If person with the crown does not accept 3 challenges in a row, crown goes to last person who challenged them

Also tracks all time wins/losses

Track amount won and lost in bets for each user all time. Including baseball bets


ACHIEVEMENTS:

Achievements awarded to users for milestones. Also gifts exclusive gacha units along with achievement
Achievements give notification in announcements in specific channel. (would also be nice if it DM'ed the user) "@Tony has earned the Trader Joe achievement!" then a picture of the unit
	Hidden achievements would be slightly different "@Tony has earned the hidden achievement, AFK" and a picture of the unit.

Command for viewing achievement list (prefer it to be DM'ed) excluding HIDDEN ACHIEVEMENTS. 
Sorted by level. Would show Name, requirement and prize. and some sort of indicator if earned
Hidden Achievements would show just the name and "???"
Requirement and prize would be visible on HIDDEN ACHIEVEMENTS that have been earned (DM only)

!setAchievement would also user to set a flair to their name. This flair would also grant bonuses in Baseball Game when set.
Only can have one flair at a time

For instance Tony *Collector*. Would be nice if different level flairs were a different color

Level 1 Achievements (gives .01 to multiplier)
Hoarder: Own 250 Units (TONPA)
Fan: Own 5 Units from the same series (KABUTO)
Collector: Own 10 UR Units (HEMULIN)
Regular Customer: Make 10 purchases in the store (BULMA(SUPER))
I'll Be Here All Week: 7-day Login Streak (MIGHT GUY)
Trader Joe: Make 15 Trades (ZEPILE)
Potion Seller: Make a trade with 5 different people (DRWILY)
Anime is Trash: Convert 50 Units (SENSUI)
Leave Me Alone: Convert the same unit 5 times (INUYASHA(DEMON))
90s Kid: Own 25 units from Dragon Ball Z, Yu Yu Hakusho, Sailor Moon, Cardcaptor Sakura, Pokemon, Revolutionary Girl Utena, Neon Genesis Evangelion, Cowboy Bebop
(MAJIN BUU)
Magical Girl: Own 15 units from Sailor Moon/Cardcaptor Sakura/Puella Magi Madoka Magicka/Revolutionary Girl Utena (SAKURA(SCHOOLUNIFORM))
Fujoshi: Own 15 units from Yuri!!! On Ice/Haikyuu/Banana Fish/Fruits Basket (SEBASTIANMICHAELIS)
Your 30-something Older Brother: Own 15 units from Cowboy Bebop/Berserk/Rurouni Kenshin/Akira (KENSHIRO)
Shonen: Own 25 units from Naruto/One Piece/Hunter x Hunter/Bleach/Demon Slayer/My Hero Academia (KIDGOKU)
Little Slugger: Win 100 Baseball Games (HARUKO(BASEBALL))
Big Check: Win Lucky Lottery 5 times (KILLUA(RISKYDICE))
Believe it!: Own 10 Naruto units (ROCK LEE(DRUNK))
Frugal: Own over 100 coins at once (BEANS)
Gamer: Own 30 Videogame Units (LAIN(BEARPAJAMA))
Zoologist: Own a Cat, Dog, and any other animal unit (KIRARA(BIG))
Contender: Take the crown from current champ in War Card Game twice (SAITAMA(CHARANKO))
DeepMind: Lose 5 times in chess (MELLO)
Curmudgeon: Reject 4 Trades (GOTOH) - HIDDEN ACHIEVEMENT
One Man's Treasure: Convert 2 UR units (HIKOSEIJURO) - HIDDEN ACHIEVEMENT
AFK: Lose crown due to inactivity (KAMINARI(DUMB)) - HIDDEN ACHIEVEMENT
Upstart: Earn 10 Achievements (SOPHIE(YOUNG)) - HIDDEN ACHIEVEMENT
Typo: Type !rol into chat (PIKACHU(FAINT)) - HIDDEN ACHIEVEMENT
Nice. : Own Gendo (#69) and Rohan Kishibe (#420) (MASTER ROSHI) - HIDDEN ACHIEVEMENT
Teacher's Pet: Type !help secret into chat (MRS FILLYJONK) - HIDDEN ACHIEVEMENT
Impatient: Use !daily command too soon 10 times. (ASUKA(SCHOOL)) - HIDDEN ACHIEVEMENT

Level 2 Achievements (gives .02 to multiplier)
Big Fan: Own 10 Units from the same series  (KENSUKE)
Connossieur: Own 25 UR Units (CHROLLO(SKILLHUNTER))
You're Going to Be a Star!: 14 Day Login Streak (KASHIRA)
Big Spender: Make 25 purchases in the store (MAXIMILLIONPEGASUS)
This is How I Win: Win 20+ coins in one bet (TONTON)
Overpowered: #1 on Leaderboard 3 consecutive times (SAITAMA(SERIOUS))
Big Slugger: Win 250 Baseball Games (NIKAIDO(BASEBALL))
World Champion: Win the Baseball World Series (GOKU (BASEBALL))
Ghibli Fanboy: Own 30 Ghibli units (FUTURE BOY CONAN)
Card Shark: Win 20 times in War Card Game (KAIJI)
Chun III: Own all three different Chun-Li units (LAUNCH)
Cat Person: Own 5 cat units (ADMIRAL)
Does Not Compute: Own 5 Robot Units (METAL SONIC)
New Horizons: Own units 491-500. (TIMMY&TOMMY)
Show-off: Set your baseball team to units earned from Achievements (DIO (YOUNG))
Reigning Champ: Win 10 times as current champ in War Card Game (BROLY)
Pokemon Trainer: Own 6 different Pokemon (SAMURAITRAINER) - HIDDEN ACHIEVEMENT
Nothing is Sacred: Convert a Achievement-Only Unit (DRGENUS) - HIDDEN ACHIEVEMENT
Monopoly: Own 5 copies of the same unit (NARUTO(SHADOWCLONE)) - HIDDEN ACHIEVEMENT
Big Boy: Earn 25 Achievements (ARMOREDSUIT(MSDFTYPE3)) - HIDDEN ACHIEVEMENT
NICE.: Own #69 Gendo, #169 Maes Hughes, #269 James, #369 Teru (Wig), #469 Dhalsim (SFII) - (VIKTORNIKIFOROV(STAYCLOSETOME)) - HIDDENACHIEVEMENT
Baseball Filler Episode: Set your Baseball Team with units from the same series (USAGI(BASEBALL)) - HIDDEN ACHIEVEMENT
Turing Test: Bet on an AI team in baseball (PROTO) - HIDDEN ACHIEVEMENT
Runner-Up: Lose the Baseball World Series (VEGETA (BASEBALL)) - HIDDEN ACHIEVEMENT

Level 3 Achievements (gives .03 to multiplier)
Coolguy: 35 day login streak (LUPIN(REDJACKET))
Weeb: Own 500 Units (MILLUKI)
Untouchable: Win 25 times as current champ in War Card Game (POMPOMPURIN)
Filthy Rich: Buy Filthy Rich achievement in store (CIELPHANTOMHIVE)
Magnus: Win 10 chess games (NIKAIDOU(HUMANSHOGI))
Triple Crown: Be the Baseball Champion/Leaderboard #1/Reigning War Game Champ at once (KAGEYAMA(KINGOFTHECOURT))
Mancub: Lose 100 Baseball Games (WEEVIL) - HIDDEN ACHIEVEMENT
Underdog: End a baseball season in last place (SHINJI(SAD)) - HIDDEN ACHIEVEMENT
This isn't even my final form: Own both Underdog and World Champion Achievements (FRIEZA(2NDFORM) - HIDDEN ACHIEVEMENT
Gentleman Thief: Own all 4 Lupin units of a different color suit (LUPIN(DISGUISE)) - HIDDEN ACHIEVEMENT
Uncut Gems: Lose 100 coins total betting (AKAGI) - HIDDEN ACHIEVEMENT
Overlord: Earn 50 Achievements (KANEDA(MOTORCYCLE)) - HIDDEN ACHIEVEMENT
Left Behind: Everyone else wins in a single Lucky Lottery but you - HIDDEN ACHIEVEMENT (BALL-CHINNED KID)


FUTURE
Boomer: Own 20 units from 80s shows...TBD (GRAN TORINO)
Critical Hit: Draw 5 UR units in War Card Game
Critical Failure: Draw 5 commmon units in War Card Game


Updated Help Menu:

Welcome to the Gacha Game Discord! Remember to check into the News Channel and Announcements Channel for updates

Collect Gacha units by using !roll command to spend tickets to pull random characters

Tickets are awarded daily- use the !daily command to claim yours. A streak of 7 daily logins in a row will give you a bonus

You may also collect tickets through the store and be awarded tickets for other reasons

Units come in 4 different rarities, Common, Rare, Super Rare and Ultra Rare

Each rarity is harder to get than the last

You can cash in unwanted units in using the !convert command for coins

For example, !convert 52. This will convert Unit #52 into coins. Higher rarities are worth more coins.

Coins can then be used to buy specific units and tickets at the store. Items in the store will change weekly.

Use !buy StoreId# to purchase a unit. For example, !buy 2

Sometimes you can earn different types of tickets. Special tickets pull from a limited group of Gacha units, this group will change overtime.
Super Tickets pull a random Super Rare Gacha Unit
Ultra Tickets pull a random Ultra Rare Gacha Unit

To spend these, type !roll special, !roll super, or !roll ultra

Use !rollAll to roll all the normal tickets you currently have

Use the !inventory command to see how many coins/tickets/units you own

Use the !collection command to view your collection of units. Use the arrow reactions to scroll through your collection. 
Use the shuffle reaction to change how it's sorted

!trade offers a trade to a user. !trade @Username IDoffered IDwanted. 

e.g !trade @mancub 1 2. This will offer mancub a trade of your Naruto for his Sasuke

Type Catalog! if you want to see a full list of the units.

If you want to see information on a specific unit and who owns it, use the !show command

i.e !show 267 or !show Pikachu

!leaderboard to see the rankings. Weekly bonuses will be given out to the leaders

Every day, a lottery will be held on a random unit. If you own that unit, you will be given a prize

You can earn achievements for various milestones. These achievements will award you exclusive Gacha units
Additionally, you can set taglines from your achievements as flair to your name with the !setflair command
Your flair can also improve your baseball team's performance. 

!achievements will generate the list of achievements. Some achievements are hidden, and the way to earn them will not be revealed
Type !help secret to earn one.

You can also see many of your stats using the !stats command

For more information on the !collection command, type !help collection

For more information on the Baseball, type !help baseball

For more information on the War Card Game, type !help war


There is also a chess bot on here! Use +help in the Chess Room to bring up its command list

(I can write up the !help baseball/war/collection prompts when the code is mostly written)
