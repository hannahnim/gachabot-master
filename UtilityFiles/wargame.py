import discord, sys, random, json
from discord.ext import commands, menus, tasks
from datetime import datetime

sys.path.append("..")  # allows UtilityFiles to import
from UtilityFiles.constants import COLOR, RARITY_MAP, DAILY_TIME, PREFIX, GRADES, \
    RARITY_VALUE  # imports settings

# imports utility functions, will be split up later
from UtilityFiles.util import get_inventory, exel_reader, get_gachas, give_card, add_card, get_pool, dump_data, \
    is_allowed, NoGachas, ShopConfirmMenu, TradeConfirmMenu, \
    GachaCard, CardMenu, CardMenuSource, ShowGachaMenu, ConvertGachaMenu, get_score, LotteryGachaMenu, AchievementMenuSource

from UtilityFiles.achievements import *

# Define global constants
GACHA_DATA = exel_reader("UtilityFiles/GACHAUNITS.xlsx")

deck1_card = 0
deck2_card = 0

# ## Every variable with a 1 is the challenger and every variable with a 2 is the challengee
# def get_all_values(d):
#     if isinstance(d, dict):
#         for v in d.values():
#             yield from get_all_values(v)
#     elif isinstance(d, list):
#         for v in d:
#             yield from get_all_values(v)
#     else:
#         yield d

def get_card(item_id):
    item_id = int(item_id)
    pool = get_pool("all")
    return [card for card in pool if item_id == card.card_id][0]

def card_to_id(deck):
    for i in range(0, len(deck)):
        deck[i] = deck[i].card_id
    return deck

def wargame_start(user1: discord.Member, user2: discord.Member):
    user1_inventory = get_inventory(str(user1.id))
    user2_inventory = get_inventory(str(user2.id))
    if len(user1_inventory) < 5:
        return False, user1
    elif len(user2_inventory) < 5:
        return False, user2
    deck1 = card_to_id(random.choices(get_gachas(user1, GACHA_DATA), k=5))
    deck2 = card_to_id(random.choices(get_gachas(user2, GACHA_DATA), k=5))
    # self.tracker[str(user1.id)][str(user2.id)] = {"deck" : deck1, "cards_drawn" : 0}
    # self.tracker[str(user2.id)][str(user1.id)] = {"deck" : deck2, "cards_drawn" : 0}
    # save(self.tracker)
    return deck1, deck2

def draw(decks):
    card1 = random.choice(decks[0])
    card2 = random.choice(decks[1])

# def battle(card1, card2):
#     power1 = card1.stats["total"]
#     power2 = card2.stats["total"]
#     if power1 == power2:
#         return None
#     elif power1 > power2:
#         return card1, ctx.author
#     else:
#         return card2, user