# Utility file which holds external classes and functions, planning on seperating this to seperate files later
import discord
from discord.ext import menus
from typing import List, Dict
from openpyxl import load_workbook
from dataclasses import dataclass

from UtilityFiles.constants import COLOR, RARITY_MAP, NO_IMAGE_URL, ROLE, RARITY_VALUE, CONVERT
import json


class NoGachas(Exception):
    pass


# made by the old bot dev not sure how it works but it returns a generator
# (There was actually an older older older bot dev. It was the first version of this Gacha code.)
# (My predecessor dev re-wrote most of his code. I believe this is some of the only stuff he kept from the old code.)
def exel_generator(filename):
    """Opens and loads an xlsx file into a generator"""
    book = load_workbook(filename)
    sheet = book.active
    rows = sheet.max_row
    cols = sheet.max_column

    def item(i, j):
        return (sheet.cell(row=3, column=j).value, sheet.cell(row=i, column=j).value)

    return (dict(item(i, j) for j in range(2, cols + 1)) for i in range(4, rows + 1))


# used the old function to create a new one that returns a dict of every gacha
def exel_reader(filename):
    result = {}
    for item in exel_generator(filename):
        id = item.pop("ID")
        result[str(id)] = item
    return result


#  Temporarily putting this global here, for Achievements.
GACHA_DATA = exel_reader("UtilityFiles/GACHAUNITS.xlsx")


def get_send_channels():
    with open("UtilityFiles/notifications.txt", "r") as txt:
        try:
            notif = int(txt.readline())
        except:
            notif = 0
    with open("UtilityFiles/newsfeed.txt", "r") as txt:
        try:
            newsfeed = int(txt.readline())
        except:
            newsfeed = 0
    return notif, newsfeed
get_send_channels()

def get_channel():
    with open("UtilityFiles/channel.txt", "r") as txt:
        out = txt.readline()
    return int(out)

def dump_data(data, file):
    """Dumps a dictionary into a json file"""
    file.seek(0)
    file.truncate(0)
    json.dump(data, file, indent=4)


def create_cache(user_id):
    """Creates a cache for a user if they have never been stored, returns an empty inventory"""
    empty_inventory = {"coins": 0, "tickets": 0, "special_tickets": 0, "super_tickets": 0, "ultra_tickets": 0, "war_game_wins" : 0, "war_game_losses" : 0,
                       "gachas": {}, "purchases": 0, "rolls": 0, "conversions": 0, "consecutive_logins": 0, "multiplier" : 0,
                       "last_login": 0,
                       "achievements": {"1":0, "2":0, "3":0, "4":0, "5":0, "6":0, "7":0, "8":0, "9":0, "10":0, "11":0,
                                        "12":0, "13":0, "14":0, "15":0, "16":0, "17":0, "18":0, "19":0, "20":0, "21":0,
                                        "22":0, "23":0, "24":0, "25":0, "26":0, "27":0, "28":0, "29":0, "30":0, "31":0,
                                        "32":0, "33":0, "34":0, "35":0, "36":0, "37":0, "38":0, "39":0, "40":0, "41":0,
                                        "42":0, "43":0, "44":0, "45":0, "46":0, "47":0, "48":0, "49":0, "50":0, "51":0,
                                        "52":0, "53":0, "54":0, "55":0, "56":0, "57":0, "58":0, "59":0, "60":0, "61":0,
                                        "62":0, "63":0, "64":0, "65":0, "66":0, "67":0}}
    # I hate this, but it's better to do this than to make sure the achievement exists in "achievements" every single
    # time in the achievements checks. Find a better alternative ASAP
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
        stored_info[str(user_id)] = empty_inventory
        dump_data(data=stored_info, file=f)

    return empty_inventory

async def give_card(ctx, user, card_id, dm = False):  # Gets a pool of all the gachas to search through
    inventory = get_inventory(str(user.id))  # gets the user's inventory

    # Admin command, so we don't need to check if a name was used instead of an ID.
    try:
        card_id = int(card_id)
        card = [card for card in pool if card_id == card.card_id][0]  # retrieves a card with the correct id
    except IndexError:  # if card[0] doesnt exist
        await ctx.send("Couldn't find the Gacha.")
        return

    gachas = inventory["gachas"]  # retrieves the user's gachas
    add_card(user, str(card.card_id))  # adds a new card to the gachas

    # finds out how many of the card the user now has
    if str(card.card_id) in gachas.keys():  # if the user has the card already
        card.amount = gachas[
                            str(card.card_id)] + 1  # card.amount is set to how many of the card the user now has
        message = f"Congratulations, you just got another {card.name}! You now have {card.amount} {card.name}'s"
    else:  # the card must be new
        card.amount = 1  # the amount is by default 1
        message = f"Congratulations, you just got a new card, {card.name}!"

    if card:  # if there is a card with the correct id
        await ctx.send(embed=discord.Embed(title=message, colour=COLOR))
        await ShowGachaMenu(card).start(ctx)  # creates a menu to show the card
        if dm:
            await user.send(embed=discord.Embed(title=message, colour=COLOR))
            await user.send(embed = card.generate_embed())
    
    return card
        
def get_achievements(user_id):
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
    try:  # tries getting the achievements from the file
        achievements = stored_info[str(user_id)]["achievements"]
    except KeyError:  # creates a cache of the user if they have never been in the file
        achievements = create_cache(user_id)
    return achievements

def get_inventory(user_id):
    if type(user_id) == discord.Member:
        user_id = user_id.id
    """Gets the inventory of a user from a user info dictionary"""
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
    try:  # tries getting the inventory from the file
        inventory = stored_info[str(user_id)]
    except KeyError:  # creates a cache of the user if they have never been in the file
        inventory = create_cache(user_id)
    return inventory


def get_score(user, excel_data):
    rarity_value = {}
    for setting in RARITY_VALUE:  # We grab the settings for how often a rarity should be put on the pool.
        rarity_value[setting.split(":")[0]] = int(setting.split(":")[1])
    try:
        cards = get_gachas(user, excel_data)
    except NoGachas:
        cards = None
    totalvalue = 0
    if cards is not None:
        for card in cards:
            value = rarity_value[card.rarity]  # Reads it's rarity.
            while card.amount:
                totalvalue += value
                card.amount -= 1
    inv = get_inventory(user.id)
    totalvalue += inv["coins"]

    return totalvalue


def is_allowed(ctx):  # A role/owner check for some commands

    groups = ctx.message.author.roles  # The user's roles
    admin = False
    for group in groups:  # Checks if any of the
        if group.permissions.manage_guild:
            admin = True
        else:
            if str(group.name).lower() == ROLE:
                admin = True

    if str(ctx.message.guild.owner_id) == str(ctx.message.author.id):
        admin = True
    if admin:
        return True
    return False

def get_card(item_id, pool):
    item_id = int(item_id)
    return [card for card in pool if item_id == card.card_id][0]

def get_coins(user):
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
    return stored_info[str(user.id)]["coins"]

def add_card(user: discord.Member, card_id: str):
    """Adds a card to a user's inventory"""
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
        if card_id in stored_info[str(user.id)]["gachas"].keys():
            stored_info[str(user.id)]["gachas"][card_id] += 1
        else:
            stored_info[str(user.id)]["gachas"][card_id] = 1
        dump_data(data=stored_info, file=f)


def remove_card(user: discord.Member, card_id: str):
    """Removes a card to a user's inventory"""
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
        if int(stored_info[str(user.id)]["gachas"][card_id]) > 1:
            stored_info[str(user.id)]["gachas"][card_id] -= 1
        else:
            stored_info[str(user.id)]["gachas"].pop(card_id)
        dump_data(data=stored_info, file=f)


@dataclass
class GachaCard:
    card_id: int
    name: str
    rarity: str
    show: str
    header: str
    descriptor: str
    ghibli: str
    image_url: str
    amount: int
    stats: Dict[str, int]

    def generate_embed(self, sort_type=None, show_footer=True):
        """Generates an embed with basic info about a card"""
        card_embed = discord.Embed(
            title=f"#**{self.card_id} | {self.name}**",
            colour=COLOR
        )
        card_embed.set_image(url=self.image_url)
        card_embed.add_field(name="**Rarity:**", value=RARITY_MAP[self.rarity])
        card_embed.add_field(name="**Show:**", value=self.show)
        if show_footer:
            if sort_type is None:
                card_embed.set_footer(text=f"Amount: {self.amount}")
            else:
                card_embed.set_footer(text=f"Amount: {self.amount}\nSort: {sort_type}")

        return card_embed

    def generate_stats_embed(self):
        """Generates an embed with stats info for a card"""
        stats_embed = discord.Embed(
            title=f"#**{self.card_id} | {self.name}**",
            colour=COLOR)
        stats_embed.set_image(url=self.image_url)
        for name, stat in self.stats.items():
            stats_embed.add_field(name=f"**{name.lower().title()}:**", value=stat, inline=False)

        return stats_embed

## Returns star ascii characters depending on achievement level
def get_star(level):
    star = "★"
    return str(star*int(level))

def get_gachas(user: discord.Member, excel_data) -> List[GachaCard]:
    gachas = get_inventory(user.id)["gachas"]
    if gachas:  # if the result isn't empty
        cards = []
        for gacha_id in gachas.keys():
            gacha_info = excel_data[gacha_id]
            try:
                card_stats = {"height": int(gacha_info["Height"]),
                              "intelligence": int(gacha_info["Intelligence"]),
                              "speed": int(gacha_info["Speed"]),
                              "fortitude": int(gacha_info["Fortitude"]),
                              "power": int(gacha_info["Power"])}
            except TypeError:
                card_stats = {"height": 0,
                              "intelligence": 0,
                              "speed": 0,
                              "fortitude": 0,
                              "power": 0}
            card_stats["total"] = sum(card_stats.values())

            card = GachaCard(
                card_id=int(gacha_id),
                name=gacha_info["CHARACTER"],
                rarity=gacha_info["Grade"],
                show=gacha_info["Show"],
                header=gacha_info["Subgroup"],
                descriptor=gacha_info["Descriptor"],
                ghibli=gacha_info["Ghibli?"],  # This one is pretty unnecessary, but it's on the excel, so why not
                image_url=gacha_info["IMAGE"] if gacha_info["IMAGE"] is not None else NO_IMAGE_URL,
                amount=int(gachas[gacha_id]),
                stats=card_stats)
            cards.append(card)

        return cards
    else:
        raise NoGachas("The user has no Gachas in their inventory")


def get_pool(rarity) -> List[GachaCard]:
    """Returns the Gacha Pool"""
    gachas = exel_reader("UtilityFiles/GACHAUNITS.xlsx")
    if rarity == "tickets":
        with open("UtilityFiles/gacha_pool.json", "r") as f:
            pool_ids = json.load(f)  # list of id #'s to make a pool
    elif rarity == "common":  # Note: Common and rare pools aren't actually made for any specific rolls, so they don't have tickets.
        with open("UtilityFiles/gacha_pool_common.json", "r") as f:
            pool_ids = json.load(f)
    elif rarity == "rare":
        with open("UtilityFiles/gacha_pool_rare.json", "r") as f:
            pool_ids = json.load(f)
    elif rarity == "special_tickets":
        with open("UtilityFiles/gacha_pool_special.json", "r") as f:
            pool_ids = json.load(f)
    elif rarity == "super_tickets":
        with open("UtilityFiles/gacha_pool_super.json", "r") as f:
            pool_ids = json.load(f)
    elif rarity == "ultra_tickets":
        with open("UtilityFiles/gacha_pool_ultra.json", "r") as f:
            pool_ids = json.load(f)
    elif rarity == "all":
        print(len(gachas))
        pool_ids = range(1, len(gachas))

    pool = []
    for pool_id in pool_ids:
        gacha_info = gachas[str(pool_id)]  # retrieves gacha with correct id
        if gacha_info["Subgroup"] is None:
            gacha_info["Subgroup"] = "Normal"
        try:
            card_stats = {"height": int(gacha_info["Height"]),
                          "intelligence": int(gacha_info["Intelligence"]),
                          "fortitude": int(gacha_info["Fortitude"]),
                          "speed": int(gacha_info["Speed"]),
                          "power": int(gacha_info["Power"])}
        except TypeError:
            card_stats = {"height": 0,
                          "intelligence": 0,
                          "fortitude": 0,
                          "speed": 0,
                          "power": 0}
        card_stats["total"] = sum(card_stats.values())
        card = GachaCard(
            card_id=int(pool_id),
            name=gacha_info["CHARACTER"],
            rarity=gacha_info["Grade"],
            show=gacha_info["Show"],
            header=gacha_info["Subgroup"],
            descriptor=gacha_info["Descriptor"],
            ghibli=gacha_info["Ghibli?"],  # This one is pretty unnecessary, but it's on the excel, so why noty
            image_url=gacha_info["IMAGE"] if gacha_info["IMAGE"] is not None else NO_IMAGE_URL,
            amount=None,
            stats=card_stats)
        pool.append(card)
    return pool

pool = get_pool("all")

def is_stat_embed(embed: discord.Embed):
    return embed.fields[0].name == "**Height:**"

def write_crown(crown):
    with open("UtilityFiles/crown.txt", "w+") as f:
        f.write(crown)

def get_crown():
    with open("UtilityFiles/crown.txt", "r+") as f:
        crown = f.readline()
    return crown

def id_from_embed(embed: discord):
    return int(embed.title[3:].split("|")[0].strip())

class AchievementMenuSource(menus.ListPageSource):
    def __init__(self, data):
        super().__init__(data, per_page=1)
        self.data = data

    async def format_page(self, menu, entries):
        return self.data[menu.current_page]

class CardMenu(menus.MenuPages):
    """Shows the collection as a menu of cards"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_sort = 0  # creates a current sort variable to sort through them

    @menus.button('🔀')
    async def on_shuffle(self, payload):
        self.current_sort += 1
        if self.current_sort >= len(SORT_LIST):  # loops back to 0 when finished going through sorts
            self.current_sort = 0

        cards = self._source.cards
        await self.change_source(CardMenuSource(cards, self.current_sort))

    @menus.button('📊', position=menus.Last(2))
    async def show_stats(self, payload):
        cards = self._source.cards
        card_id = id_from_embed(self.message.embeds[0])  # parses the embed and fetches the card id
        current_card = [card for card in cards if card.card_id == card_id][0]

        if is_stat_embed(self.message.embeds[0]):  # if the embed is a stats embed
            await self.message.edit(embed=current_card.generate_embed())
        else:  # if the embed is a regular embed
            await self.message.edit(embed=current_card.generate_stats_embed())

    # necessary to override the default black square in discord.Menus
    @menus.button('\N{BLACK SQUARE FOR STOP}\ufe0f', position=menus.Last(3))
    async def stop_pages(self, payload):
        """stops the pagination session."""
        self.stop()

class CardMenuSource(menus.ListPageSource):
    """Source for CardMenu"""
    def __init__(self, cards: GachaCard, current_sort: int = 0):
        self.current_sort = current_sort
        self.cards = SORT_LIST[self.current_sort](cards)  # calls the current sort func on the cards list
        super().__init__([*range(len(cards))], per_page=1)

    async def format_page(self, menu, page):
        return self.cards[page].generate_embed(SORT_LIST[self.current_sort].__doc__)


class ShowGachaMenu(menus.Menu):
    """Creates a menu for the show command"""

    def __init__(self, card, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.card = card  # creates a current sort variable to sort through them

    async def send_initial_message(self, ctx, channel):
        return await channel.send(embed=self.card.generate_embed())

    @menus.button('📊', position=menus.Last(2))
    async def show_stats(self, payload):
        if is_stat_embed(self.message.embeds[0]):  # if the embed is a stats embed
            await self.message.edit(embed=self.card.generate_embed())
        # if the embed is a regular embed
        else:
            await self.message.edit(embed=self.card.generate_stats_embed())


class LotteryGachaMenu(menus.Menu):
    """Creates a menu for the show command"""

    def __init__(self, card, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.card = card  # creates a current sort variable to sort through them

    async def send_initial_message(self, ctx, channel):
        return await channel.send(embed=self.card.generate_embed(None, False))


class ShopConfirmMenu(menus.Menu):
    """Creates a menu for the show command"""

    def __init__(self, ctx, item, is_gacha, *args, **kwargs):
        super().__init__(timeout=30.0, delete_message_after=True, *args, **kwargs)
        self.ctx = ctx
        self.item = item
        self.is_gacha = is_gacha
        self.confirmation = None

    async def send_initial_message(self, ctx, channel):
        if self.is_gacha:
            self.confirmation = await channel.send(
                embed=discord.Embed(title=f"You are about to buy {self.item.name}. React with ✔ to confirm.",
                                    colour=COLOR))
            return await channel.send(embed=self.item.generate_embed())
        else:
            return await channel.send(
                embed=discord.Embed(title=f"You are about to buy a {self.item}. React with ✔ to confirm.",
                                    colour=COLOR))

    @menus.button(emoji='📊')
    async def show_stats(self, payload):
        if self.is_gacha:
            if is_stat_embed(self.message.embeds[0]):  # if the embed is a stats embed
                await self.message.edit(embed=self.item.generate_embed())
            # if the embed is a regular embed
            else:
                await self.message.edit(embed=self.item.generate_stats_embed())

    @menus.button('❌', position=menus.Last(2))
    async def cancel(self, payload):
        await self.message.delete()
        if self.is_gacha:
            await self.confirmation.delete()
        return await self.message.channel.send(
            embed=discord.Embed(title=f"Purchase canceled.", colour=COLOR))
        self.stop()

    @menus.button('✔️', position=menus.Last(3))
    async def confirm(self, payload):
        inventory = get_inventory(self.ctx.author.id)
        if self.is_gacha:
            gachas = inventory["gachas"]  # retrieves the user's gachas
            add_card(self.ctx.author, str(self.item.card_id))  # adds a new card to the gachas

            # finds out how many of the card the user now has
            if str(self.item.card_id) in gachas.keys():  # if the user has the card already
                self.item.amount = gachas[
                                       str(
                                           self.item.card_id)] + 1  # card.amount is set to how many of the card the user now has
                message = f"Congratulations, you just got another {self.item.name}! You now have {self.item.amount} {self.item.name}'s"
            else:  # the card must be new
                self.item.amount = 1  # the amount is by default 1
                message = f"Congratulations, you just got a new card, {self.item.name}!"

            if self.item:  # if there is a card with the correct id
                await self.message.delete()
                await self.confirmation.delete()
                await self.ctx.send(embed=discord.Embed(title=message, colour=COLOR))
                await ShowGachaMenu(self.item).start(self.ctx)  # creates a menu to show the card
        else:
            # add card, remove tickets here
            await self.message.delete()
            await self.message.channel.send(embed=discord.Embed(
                title=f"You purchased a {self.item}.",
                colour=COLOR))
            if self.item == "Ticket":
                self.item = "tickets"
            elif self.item == "Special Ticket":
                self.item = "special_tickets"
            elif self.item == "Super Ticket":
                self.item = "super_tickets"
            elif self.item == "Ultra Ticket":
                self.item = "ultra_tickets"
        with open("UtilityFiles/user_info.json", "r+") as f:
            stored_user_info = json.load(f)
            if not self.is_gacha:
                stored_user_info[str(self.ctx.author.id)][self.item] += 1
            stored_user_info[str(self.ctx.author.id)]["coins"] = inventory["coins"]
            stored_user_info[str(self.ctx.author.id)]["purchases"] += 1
            dump_data(data=stored_user_info, file=f)
        store_achievement_checker(self.ctx)
        self.stop()


class ConvertGachaMenu(menus.Menu):
    """Creates a menu for the show command"""

    def __init__(self, user, card, *args, **kwargs):
        super().__init__(timeout=30.0, delete_message_after=True, *args, **kwargs)
        self.card = card  # creates a current sort variable to sort through them
        self.user = user
        self.inventory = get_inventory(user.id)
        rates = {}
        for setting in CONVERT:
            rates[setting.split(":")[0]] = int(setting.split(":")[1])
        self.value = rates[card.rarity]  # Reads it's rarity.
        self.confirmation = None

    async def send_initial_message(self, ctx, channel):
        self.confirmation = await channel.send(
            embed=discord.Embed(title=f"Are you sure you want to convert {self.card.name} into {self.value} coin(s)?\
                                                            \nReact with ✔ to proceed.", colour=COLOR))
        return await channel.send(embed=self.card.generate_embed())

    @menus.button('📊')
    async def show_stats(self, payload):
        if is_stat_embed(self.message.embeds[0]):  # if the embed is a stats embed
            await self.message.edit(embed=self.card.generate_embed())
        # if the embed is a regular embed
        else:
            await self.message.edit(embed=self.card.generate_stats_embed())

    @menus.button('❌', position=menus.Last(2))
    async def cancel(self, payload):
        await self.message.delete()
        await self.confirmation.delete()
        return await self.message.channel.send(
            embed=discord.Embed(title=f"Conversion canceled.", colour=COLOR))
        self.stop()

    @menus.button('✔️', position=menus.Last(3))
    async def confirm(self, payload):
        remove_card(self.user, str(self.card.card_id))
        await self.message.delete()
        await self.confirmation.delete()
        await self.message.channel.send(embed=discord.Embed(
            title=f"Successfully turned {self.card.name} into {self.value} coin(s)! You now have {self.inventory['coins'] + self.value} coins.",
            colour=COLOR))
        with open("UtilityFiles/user_info.json", "r+") as f:
            stored_info = json.load(f)
            print("AHOY MATEY, 'TIS ME, THE TESTING PRINT! ARR!")
            stored_info[str(self.user.id)]["coins"] += self.value
            stored_info[str(self.user.id)]["conversions"] += 1
            dump_data(stored_info, f)
        self.stop()


class TradeConfirmMenu(menus.Menu):

    def __init__(self, user: discord.Member, card1, card2, *args, **kwargs):
        super().__init__(timeout=30.0, delete_message_after=True, *args, **kwargs)
        self.oguser = user
        self.card1 = card1
        self.card2 = card2

    async def send_initial_message(self, ctx, channel):
        return await channel.send(embed=generate_embed_trade(self))

    @menus.button('❌')
    async def cancel(self, payload):
        await self.message.delete()
        return await self.message.channel.send(
        embed=discord.Embed(title=f"Trade rejected.", colour=COLOR))
        await trade_achievement_checker(ctx, user, self.ctx.author, oguser, False)
        self.stop()

    @menus.button('✔️', position=menus.Last(2))
    async def confirm(self, payload):
        remove_card(self.ctx.author, str(self.card2.card_id))
        add_card(self.ctx.author, str(self.card1.card_id))
        remove_card(self.oguser, str(self.card1.card_id))
        add_card(self.oguser, str(self.card2.card_id))

        await self.message.delete()
        await self.message.channel.send(embed=discord.Embed(
            title=f"Successfully traded {self.card1.name} and {self.card2.name}!",
            colour=COLOR))
        self.stop()


def generate_embed_trade(self):
    """Generates an embed with basic info about a card"""
    card_embed = discord.Embed(
        title=f"**Trade Request!**\nReact with ✔ to accept.\n{self.oguser.display_name}'s card: #{self.card1.card_id} | {self.card1.name}\n{self.ctx.author.display_name}'s card: #{self.card2.card_id} | {self.card2.name}",
        colour=COLOR
    )
    card_embed.set_image(url=self.card1.image_url)
    card_embed.set_thumbnail(url=self.card2.image_url)
    card_embed.add_field(name=f"**{self.card1.name}'s rarity:**", value=RARITY_MAP[self.card1.rarity])
    card_embed.add_field(name=f"**{self.card2.name}'s rarity:**", value=RARITY_MAP[self.card2.rarity])

    return card_embed


class Sorts:  # note: each function's docstring is used to show the current sort
    """Class that organizes functions used for sorting the gacha collection"""

    @staticmethod
    def id_sort(cards):
        """Number"""  # sorts by card id number
        return sorted(cards, key=lambda x: x.card_id)
    
    @staticmethod
    def power_sort(cards):
        """Total Power"""
        return sorted(cards, key=lambda x: x.stats["total"], reverse=True)

    @staticmethod
    def name_sort(cards):
        """Name"""  # sorts by gacha name, alphabetically
        return sorted(cards, key=lambda x: x.name)

    @staticmethod
    def rarity_sort(cards):
        """Rarity"""  # sorts by rarity first (C, R, SR, UR), then id
        rarity_dict = {"C": [], "R": [], "SR": [], "UR": []}

        for card in cards:
            rarity_dict[card.rarity].append(card)

        sorted_cards = []
        for card_lists in rarity_dict.values():
            card_lists.sort(key=lambda x: x.card_id)
            sorted_cards += card_lists
        return sorted_cards

    @staticmethod
    def show_sort(cards):
        """Show"""  # sorts by show (alphabetically), then id
        show_dict = {}
        for card in cards:
            if card.show in show_dict.keys():
                show_dict[card.show].append(card)
            else:
                show_dict[card.show] = [card]
        # sorts the dictionary
        show_dict = {show: card_lists for show, card_lists in sorted(show_dict.items())}

        sorted_cards = []
        for card_lists in show_dict.values():
            card_lists.sort(key=lambda x: x.card_id)
            sorted_cards += card_lists
        return sorted_cards

    @staticmethod
    def amount_sort(cards):
        """Amount"""  # sorts by amount of cards held
        return sorted(cards, key=lambda x: x.amount)


SORT_LIST = [Sorts.id_sort, Sorts.name_sort, Sorts.rarity_sort, Sorts.show_sort, Sorts.amount_sort, Sorts.power_sort]
