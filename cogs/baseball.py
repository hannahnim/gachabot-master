import discord, sys, random, json
from discord.ext import commands, menus, tasks
from datetime import datetime, timedelta
import asyncio
from pytz import timezone
from copy import deepcopy

sys.path.append("..")  # allows UtilityFiles to import
from UtilityFiles.constants import COLOR, RARITY_MAP, DAILY_TIME, PREFIX, GRADES, \
    RARITY_VALUE  # imports settings

# imports utility functions, will be split up later
from UtilityFiles.util import get_inventory, exel_reader, get_gachas, give_card, add_card, get_pool, dump_data, \
    get_card, get_send_channels, \
    is_allowed, NoGachas, ShopConfirmMenu, TradeConfirmMenu, \
    GachaCard, CardMenu, CardMenuSource, ShowGachaMenu, ConvertGachaMenu, get_score, LotteryGachaMenu, \
    AchievementMenuSource, pool

from UtilityFiles.achievements import *

# Define global constants
GACHA_DATA = exel_reader("UtilityFiles/GACHAUNITS.xlsx")
notif, newsfeed = get_send_channels()

with open("UtilityFiles/baseball_tracker.json", "r+") as out:
    tracker = json.load(out)

counter = tracker["counter"]


# Just makes saving easier. Takes the entire tracker dict as a parameter and saves it
def save(tracker):
    with open("UtilityFiles/baseball_tracker.json", "r+") as out:
        stored_info = json.load(out)
        stored_info = tracker
        dump_data(data=stored_info, file=out)


# The key in the baseball_tracker.json corresponds to the game counter. Going up from 1. If it cant find an entry with
# the current number itll make a new template
if str(counter) not in tracker.keys():
    tracker[str(counter)] = {
        "state": 0,
        "name_counter": 0,
        "gamenum": 0,
        "times": {},
        "events": {"preseason_warn": False, "teamlock": False, "seasonstart": False,
                   "semi_games": False, "bet_reminder": False, "finals_bet_reminder": False, "bet": False},
        "Teams": {}
    }
    save(tracker)

# Empty team dict for the ai_team, team set and team random functions
empty_team = {
    "pitcher": {"gacha": 0, "power": 0},
    "catcher": {"gacha": 0, "power": 0},
    "first base": {"gacha": 0, "power": 0},
    "second base": {"gacha": 0, "power": 0},
    "third base": {"gacha": 0, "power": 0},
    "shortstop": {"gacha": 0, "power": 0},
    "left outfield": {"gacha": 0, "power": 0},
    "center outfield": {"gacha": 0, "power": 0},
    "right outfield": {"gacha": 0, "power": 0},
    "coach": {"gacha": 0, "power": 0}
}

date_format = '%H:%M %d.%m.%y'


def get_all_values(d):
    if isinstance(d, dict):
        for v in d.values():
            yield from get_all_values(v)
    elif isinstance(d, list):
        for v in d:
            yield from get_all_values(v)
    else:
        yield d


# Reloads the tracker file and returns it.
def reload_tracker():
    with open("UtilityFiles/baseball_tracker.json", "r+") as out:
        stored_info = json.load(out)
        tracker = stored_info
    return tracker


# All the event timers are pre calculated and stored in the dict as strings. This function takes them and returns them
# in a list as date time objects
def get_times():
    d = tracker[str(counter)]["times"]
    times = {key: datetime.strptime(time, date_format) for key, time in d.items()}
    return times


# I dont even really remember what this is used for
def dict_to_index(dict, index):
    list(dict.keys()).index(index)

# Gets the current time as a string
def get_date():
    # newyork = timezone("America/New_York")
    # ny_time = datetime.now(newyork)
    ny_time = datetime.now()
    now = ny_time.strftime(date_format)
    return now

# Get the date but as a datetime object
def get_raw_date():
    # newyork = timezone("America/New_York")
    # ny_time = datetime.now(newyork)
    return datetime.now()


# converts from datetime to string and back
def convert(date):
    if type(date) == str:
        return datetime.strptime(date, date_format)
    else:
        return date.strftime(date_format)

# Gets the team of the given user id
def get_team(user):
    return tracker[str(counter)][str(user.id)]["Team"]

# Starts the season. Calculates all the season timers, sets the state and saves it
def start_season():
    start_date = get_raw_date()
    if start_date.hour > 14:
        time_offset = 3
    else:
        time_offset = 2
    preseason_end = start_date + timedelta(days=time_offset)
    preseason_end = preseason_end.replace(hour=14, minute=0)
    semi_matches = preseason_end + timedelta(days=9)
    semi_matches.replace(hour=16, minute=0)
    finals = semi_matches + timedelta(days=2)
    tracker[str(counter)]["state"] = "preseason"
    tracker[str(counter)]["times"] = {"startdate": convert(start_date), "preseason_end": convert(preseason_end),
                                      "semi_matches": convert(semi_matches), "finals": convert(finals)}
    save(tracker)
    return start_date

# This needs adding to. Currently just increases the counter by one which will result in KeyErrors
def stop_season():
    tracker["counter"] += 1
    save(tracker)


# Stat distribution for the different roles. Used for calculating power and displaying teams
stat_dist = {
    "pitcher": ["height", "speed", "power"],
    "catcher": ["intelligence", "fortitude"],
    "first base": ["height", "fortitude"],
    "second base": ["speed", "intelligence"],
    "third base": ["speed", "fortitude"],
    "shortstop": ["height", "intelligence"],
    "left outfield": 0,
    "center outfield": 0,
    "right outfield": 0,
    "coach": ["intelligence", "intelligence"]
}


# Calculate the power of a given card. Consult the txt for more info on all the mechanics
def calculate_power(card, role):
    field_power = 0
    if type(card) != GachaCard:
        card = get_card(card, pool)
    try:
        for stat in stat_dist[role.lower()]:
            field_power += card.stats[stat]
        field_power += card.stats["power"]
    except TypeError:
        field_power = 0
    return field_power


def get_power(team, random_factor, multiplier=0):
    total_power = 0
    print(team)
    for player in team.values():
        total_power += player["power"]
    total_power = total_power * random_factor + multiplier
    return total_power


# Determines the message to be sent. Check the txt
def determine_msg(random_factor, winner, loser, difference):
    loser_name = loser["name"]
    winner_name = winner["name"]
    winner = winner["team"]
    loser = loser["team"]
    messages1 = [f'after {loser_name} forgets their bats at home',
                 f'after Pitcher {loser["pitcher"]["gacha"]} breaks both arms mid-game',
                 f'- {loser_name} reportedly sick after eating a bad batch of oysters last night',
                 f'- Locker room interview reveals Shortstop {loser["shortstop"]["gacha"]} of {loser_name} cursed by powerful demon',
                 f'- Coach {loser["coach"]["gacha"]} claims {winner_name} was cheating']
    messages2 = [f'{winner_name} receives rulers blessing.',
                 f'Outfielder {winner["left outfield"]["gacha"]} of {winner_name} goes super saiyan at the plate.',
                 f'1st Baseman {winner["first base"]["gacha"]} reportedly trained under a waterfall for 3 months prior to game',
                 f'Unconfirmed claims of Catcher {winner["catcher"]["gacha"]} opening Maw of Hell in opposing {loser_name}\'s dugout',
                 f'Plot Armor proves {winner_name} to be protagonist of this season.']
    if 1.05 < random_factor < 1.08:
        msg = random.choice(messages1)
    elif 1.17 < random_factor < 1.2:
        msg = random.choice(messages2)
    elif difference >= 300:
        msg = f'{winner_name} CRUSHES {loser_name}'
    elif difference <= 50:
        msg = f'{winner_name} narrowly edges out {loser_name}'
    else:
        msg = f'{winner_name} defeats {loser_name}'
    return msg


# This is bad. I was in a trance writing this. basically just takes two teams and sees who wins and returns an embed to
# be sent. We dont need to check for draws because theyre statistically impossible.
def fight(team1_id, team2_id, playoffs=False):
    random_factor1 = random.uniform(1.05, 1.2)
    random_factor2 = random.uniform(1.05, 1.2)
    team1_info = tracker[str(counter)]["Teams"][team1_id]
    team1 = tracker[str(counter)]["Teams"][team1_id]["team"]
    team1_name = tracker[str(counter)]["Teams"][team1_id]["name"]
    team2_info = tracker[str(counter)]["Teams"][team2_id]
    team2 = tracker[str(counter)]["Teams"][team2_id]["team"]
    team2_name = tracker[str(counter)]["Teams"][team2_id]["name"]
    power1 = get_power(team1, random_factor=random_factor1)
    power2 = get_power(team2, random_factor=random_factor2)
    fight_results = {"winner": 0, "msg": 0, "power1": power1, "power2": power2}
    difference = abs(power1 - power2)  # Calculates the absolute value
    if power1 > power2:
        winner = team1_id
        loser = team2_id
        fight_results["winner"] = team1
        fight_results["winner_name"] = team1_name
        fight_results["loser"] = team2
        fight_results["loser_name"] = team2_name
        fight_results["msg"] = determine_msg(random_factor1, team1_info, team2_info, difference)
        team1_info["wins"] += 1
        team2_info["losses"] += 1
    elif power1 < power2:
        winner = team2_id
        loser = team1_id
        fight_results["winner"] = team2
        fight_results["winner_name"] = team2_name
        fight_results["loser"] = team1
        fight_results["loser_name"] = team1_name
        fight_results["msg"] = determine_msg(random_factor2, team2_info, team1_info, difference)
        team2_info["wins"] += 1
        team1_info["losses"] += 1
    results = fight_results
    save(tracker)
    embed = discord.Embed(title=f'{team1_name} VS {team2_name}', colour=COLOR)
    embed.add_field(name="Winner", value=results["winner_name"])
    embed.add_field(name="Score", value=f'{round(results["power1"])} vs {round(results["power2"])}', inline=True)
    embed.add_field(name="Message", value=results["msg"], inline=False)
    img = get_card(results["winner"]["coach"]["gacha"], pool).image_url
    embed.set_thumbnail(url=img)
    if playoffs == True:
        return winner, loser, embed
    return embed, winner


events = tracker[str(counter)]["events"]


def get_standings(e=True):
    tracker = reload_tracker()
    teams = tracker[str(counter)]["Teams"]
    teams_sort = sorted(list(teams.keys()), key=lambda x: int(teams[x]["wins"]), reverse=True)
    msg = discord.Embed(title="Standings", colour=COLOR)
    if e == False:
        return teams_sort[0:4]
    for i, teamnum in enumerate(teams_sort):
        teamnum = str(teamnum)
        name = teams[teamnum]["name"]
        wins = teams[teamnum]["wins"]
        losses = teams[teamnum]["losses"]
        msg.add_field(name=name, value=f"Wins: {wins}  Losses: {losses}", inline=False)
    return msg


# Gets an embed to send to show the playoff messages. Place 1vs4 and 2vs 3
def get_playoffs():
    teams = tracker[str(counter)]["Teams"]
    t = get_standings(e=False)
    msg = discord.Embed(title="Playoff Matches", colour=COLOR)
    msg.add_field(name="Game 1", value=f'{teams[t[0]]["name"]} VS. {teams[t[3]]["name"]}')
    msg.add_field(name="Game 2", value=f'{teams[t[1]]["name"]} VS. {teams[t[2]]["name"]}')
    return msg


# Very simple round robin algorithm thats infinitely scalable given an equal number of teams. Since were always adding
# AI teams to fill out the roster we should always have an equal number.
def get_matches(teams):
    teams = list(teams.keys())
    teams_copy = teams.copy()
    matches = []
    for team in teams:
        for team_copy in teams_copy:
            if team == team_copy:
                continue
            match = [team, team_copy]
            matches.append(match)
    # tracker[str(counter)]["matches"] = matches
    # save(tracker)
    return matches


#def get_state():
    #pass


# Quick function to update the user file with their winnings
def update_user(user_id, coins, tickets, special_tickets, super_tickets, ultra_tickets):
    if int(user_id) > 200:
        return
    with open("UtilityFiles/user_info.json", "r+") as f:
        stored_info = json.load(f)
        stored_info[str(user_id)]["coins"] += coins
        stored_info[str(user_id)]["tickets"] += tickets
        stored_info[str(user_id)]["special_tickets"] += special_tickets
        stored_info[str(user_id)]["super_tickets"] += super_tickets
        stored_info[str(user_id)]["ultra_tickets"] += ultra_tickets
        dump_data(data=stored_info, file=f)


def startmsg():
    teams = tracker[str(counter)]["Teams"]
    l = []
    sep = "\n"
    try:
        winner = tracker[str(counter - 1)]["winner"]["name"]
    except:
        winner = "no one"
    for value in teams.values():
        l.append(value["name"])
    msg = f"""Welcome to opening day of season {counter} of Gacha Bot Baseball

Our teams this season are:
{sep.join(l)}

Last season our winner was {winner}
Good luck to all this season!"""
    return msg


# state = get_state()
# channel = tracker["channel"]
teamlock = events["teamlock"]


def lock(ctx):
    return teamlock == False


date_compare = "%H %d"



# def save_state(state):
#     tracker[]

class Baseball(commands.Cog):

    # Only starts the loop when it can find a date in the tracker. Without this the loop would start and immediately fail
    # because of a KeyError
    def __init__(self, bot):
        self.bot = bot
        if "preseason_end" in tracker[str(counter)]["times"]:
            self.game_loop.start()

    # Just sends an embed with the standings
    @commands.command(name="st")
    async def standings_cmd(self, ctx):
        m = get_standings(e=False)
        await ctx.send(embed=m)

    # Testing command. Randomizes the wins and losses of each team
    @commands.command()
    async def winr(self, ctx):
        for teamnum, team in tracker[str(counter)]["Teams"].items():
            team["wins"] = random.randint(0, 20)
            team["losses"] = random.randint(0, 20)
        save(tracker)

    # Testing command. Prints a fight of every single match
    @commands.command(name="match")
    async def matches_cmd(self, ctx):
        tracker = reload_tracker()
        teams = tracker[str(counter)]["Teams"]
        matches = get_matches(teams)
        for match in matches:
            msg = fight(match[0], match[1])
            await ctx.send(embed=msg)

    # TEsting command. Obsolete
    @commands.command(name="testmatch")
    async def testmatch_cmd(self, ctx):
        teams = await self.create_ai_teams(2)
        random_factor = random.uniform(1.05, 1.2)
        results = fight(teams, random_factor)
        winner = results["winner"]
        loser = results["loser"]
        embed = discord.Embed(title=f'{teams[0]["name"]} VS {teams[1]["name"]}', colour=COLOR)
        embed.add_field(name="Winner", value=results["winner_name"])
        embed.add_field(name="Score", value=f'{round(results["power1"])} vs {round(results["power2"])}', inline=True)
        embed.add_field(name="Message", value=results["msg"], inline=False)
        await ctx.send(embed=embed)
        # print(fight(teams, random_factor))

    # The main game loop. Discord.py is planning on adding a scheduling module but until then I believe this to be the
    # safest and most optimal solution. Runs every 5 seconds and does all time related checks. Works on an event system
    # with bools and datetime objects.
    @tasks.loop(seconds=5.0)
    async def game_loop(self):
        self.newsfeed = self.bot.get_channel(newsfeed)
        self.notif = self.bot.get_channel(notif)
        now = datetime.now()
        times = get_times()
        pre_end = times["preseason_end"]
        semi_matches = times["semi_matches"]
        finals = times["finals"]
        teamlock = events["teamlock"]
        semi_games = events["semi_games"]
        bet_reminder = events["bet_reminder"]
        finals_bet_reminder = events["finals_bet_reminder"]
        gamenum = tracker[str(counter)]["gamenum"]
        matches = tracker[str(counter)]["matches"]
        gamesplayed_today = tracker[str(counter)]["gamesplayed_today"]
        state = tracker[str(counter)]["state"]
        if now.hour == (pre_end - timedelta(hours=12)).hour and events["preseason_warn"] is False:
            await self.newsfeed.send(
                "Season officially starts in 12 hours, please set your team if you haven't already")
            events["preseason_warn"] = True
            save(tracker)
        elif now.hour > (pre_end - timedelta(hours=1)).hour and teamlock is False:
            teamlock = True
            save(tracker)
        elif now.hour == pre_end.hour and events["seasonstart"] is False:
            e = discord.Embed(colour=COLOR)
            e.add_field(name=f"Season {counter} Start!", value=startmsg())
            self.newsfeed.send(embed=e)
            state = "games"
            save(tracker)
        if state == "games":
            if now.hour == 14 and gamesplayed_today == 0:
                temp = matches[gamenum]
                results, winner = fight(temp[0], temp[1])
                await self.newsfeed.send(embed=results)
                # Check txt for winnings.
                update_user(winner, 1, 0, 0, 0, 0)
                gamesplayed_today += 1
                gamenum += 1
                save(tracker)
            elif now.hour == 20 and gamesplayed_today == 1:
                temp = matches[gamenum]
                results, winner = fight(temp[0], temp[1])
                await self.newsfeed.send(embed=results)
                update_user(winner, 1, 0, 0, 0, 0)
                gamesplayed_today += 1
                gamenum += 1
                save(tracker)
                if matches[gamenum - 1] == matches[-1]:
                    await self.newsfeed.send("The Top 4 have been decided!")
                    await self.newsfeed.send(embed=get_playoffs())
                    await self.newsfeed.send("You have until tomorrow to bet for your favourites")
                    state = "semi-finals"
                    save(tracker)
                else:
                    await self.newsfeed.send(embed=get_standings())
        if state == "semi-finals":
            if now.hour == (semi_matches.hour - 12) and bet_reminder == False:
                self.newsfeed.send("You have 12 hours left to bet on your favourite team!")
                bet_reminder = True
                save(tracker)
            if now.hour == semi_matches.hour and semi_games == False:
                teamnums = get_standings(e=False)
                winner1, loser1, results = fight(teamnums[0], teamnums[3], playoffs=True)
                update_user(loser1, 3, 3, 0, 0, 0)
                results.title = "Playoff Match 1"
                results.set_thumbnail(url=discord.Embed.Empty)  # removes the thumbnail
                await self.newsfeed.send(results)
                teamnums = get_standings(e=False)
                winner2, loser2, results = fight(teamnums[1], teamnums[2], playoffs=True)
                update_user(loser2, 3, 3, 0, 0, 0)
                results.title = "Playoff Match 2"
                results.set_thumbnail(url=discord.Embed.Empty)  # removes the thumbnail
                await self.newsfeed.send(results)
                await self.newsfeed.send("You have until the next match to set your bets")
                state = "finals"
                finalists = [winner1, winner2]
                tracker[str(counter)]["finalists"] = finalists
                save(tracker)
        if state == "finals":
            if now.hour == finals.hour - 12 and finals_bet_reminder == False:
                await self.newsfeed.send("You have 12 hours to bet on the finals matches")
                finals_bet_reminder = True
            if now.hour == finals.hour:
                finalists = tracker[str(counter)]["finalists"]
                winner, loser, results = fight(finalists[0], finalists[1], playoffs=True)
                update_user(winner, 10, 10, 3, 0, 2)
                update_user(loser, 4, 4, 2, 2, 0)
                await self.newsfeed.send(embed=results)
        # elif now.hour == pre_end.hour and now.day == pre_end.day:

        # if state == "games" and now.hour == 14 or now.hour == 20:
        #     pass
        #     #TODO

        # elif now.hour == pre_end.hour and now.day == pre_end.day:

        # if state == "games" and now.hour == 14 or now.hour == 20:
        #     pass
        #     #TODO

    # Starts the season and sets all values and then starts the game loop
    @commands.command(name="startseason")
    async def start_cmd(self, ctx):
        self.newsfeed = self.bot.get_channel(newsfeed)
        self.notif = self.bot.get_channel(notif)
        start_season()
        times = get_times()
        preseason_end = times["preseason_end"]
        await self.newsfeed.send(f"Welcome to Season {counter} of Gacha Bot Baseball!")
        await self.newsfeed.send(
            f'Preseason starts now, you have until 2PM {preseason_end.strftime("%A")} EST to set your team and team name.')
        self.game_loop.start()

    # Incomplete
    @commands.command(name="bet")
    async def bet_cmd(self, ctx):
        bet = events["bet"]
        if bet == False:
            await ctx.send("You cannot bet yet.")
        else:
            await ctx.send("Which team do you want to bet on?")

    # Creates the given amount of AI teams. Pulls names from a txt file
    @commands.command(name="ai")
    async def create_ai_teams(self, ctx, amount):
        teams = []
        batting_power = 0
        with open("UtilityFiles/baseball_teams.txt", "r") as f:
            for line in f.readlines():
                teams.append(line.replace("\n", ""))
        name_counter = tracker[str(counter)]["name_counter"]
        k = 1
        for j in range(0, int(amount)):
            team = deepcopy(empty_team)
            choices = random.choices(pool, k=10)
            for key, gacha in zip(team.keys(), choices):
                team[key]["gacha"] = gacha.card_id
                team[key]["power"] = calculate_power(gacha, key)
                batting_power += gacha.stats["power"]
            tracker[str(counter)]["Teams"][str(k)] = {"name": f"{teams[name_counter]}'s Team", "wins": 0, "losses": 0,
                                                      "batting_power": batting_power, "team": team}
            k += 1
            name_counter += 1
            tracker[str(counter)]["name_counter"] = name_counter
        save(tracker)

    #Creates two ai teams and makes them fight
    @commands.command(name="testmatch")
    async def testmatch_cmd(self, ctx):
        teams = await self.create_ai_teams(2)
        random_factor = random.uniform(1.05, 1.2)
        results = fight(teams, random_factor)
        winner = results["winner"]
        loser = results["loser"]
        embed = discord.Embed(title=f'{teams[0]["name"]} VS {teams[1]["name"]}', colour=COLOR)
        embed.add_field(name="Winner", value=results["winner_name"])
        embed.add_field(name="Score", value=f'{round(results["power1"])} vs {round(results["power2"])}', inline=True)
        embed.add_field(name="Message", value=results["msg"], inline=False)
        await ctx.send(embed=embed)
        # print(fight(teams, random_factor))

    @commands.group()
    @commands.check(lock)
    async def team(self, ctx):
        pass

    # Set the name of your team
    @team.command(name="name")
    async def team_name_cmd(self, ctx):
        def user_check(m):
            return m.author == ctx.author

        await ctx.send("Choose your team's name")
        m = await self.bot.wait_for("message", check=user_check)
        tracker[str(counter)]["Teams"][str(ctx.author.id)]["name"] = m.content
        save(tracker)
        await ctx.send("New name has been set")

    # Set your team. Just run it to see how it works.
    @team.command(name="set")
    async def team_set_cmd(self, ctx):
        gachas = get_gachas(ctx.author, GACHA_DATA)
        if len(gachas) < 10:
            await ctx.send("You do not have enough gachas to make a team")
        initial_msg = discord.Embed(colour=COLOR, title=f"Choose your team {ctx.author.name}!")
        initial_msg.set_footer(text="Type in \"exit\" to exit at any point")
        embed_msg = await ctx.send(embed=initial_msg)
        team = deepcopy(empty_team)
        index = 0
        for key in team.keys():
            role = key.title()
            embed = initial_msg
            embed.add_field(name=f"Choose your {role}!", value="???")
            await embed_msg.edit(embed=embed)

            def check(m):
                return m.author == ctx.author

            duplicate = True
            while duplicate:
                msg = await self.bot.wait_for("message", check=check)
                if msg.content.lower() == "exit":
                    e = await ctx.send("Exiting...")
                    await asyncio.sleep(2)
                    await e.delete()
                    await embed_msg.delete()
                    return
                for gacha in gachas:
                    if msg.content.lower() in gacha.name.lower() or msg.content in str(gacha.card_id):
                        if gacha.card_id in get_all_values(team):
                            await ctx.send("You already used that gacha! Enter a new one.")
                            duplicate = True
                            break
                        add_msg = await ctx.send(f"{gacha.name} has been added as your {role}")
                        await asyncio.sleep(1)
                        await add_msg.delete()
                        await msg.delete()
                        print(team)
                        team[role.lower()]["gacha"] = gacha.card_id
                        team[role.lower()]["power"] = calculate_power(gacha, role)
                        embed.set_field_at(index, name=role, value=gacha.name)
                        await embed_msg.edit(embed=embed)
                        index += 1
                        duplicate = False
                        break
                    elif gacha == gachas[-1]:
                        await ctx.send("Could not find gacha. Please try again")
                        duplicate = True
                        break

        def user_check(m):
            return m.author == ctx.author

        if tracker[str(counter)]["Teams"][str(ctx.author.id)]["name"] != "":
            await ctx.send("Choose your team's name")
            m = await self.bot.wait_for("message", check=user_check)
            team_name = m.content
            tracker[str(counter)]["Teams"][str(ctx.author.id)] = {"name": team_name, "wins": 0, "losses": 0,
                                                                  "team": team}
            save(tracker)
        else:
            msg = await ctx.send("Do you want to change your team name?")
            await msg.add_reaction("👍")
            await msg.add_reaction("👎")

            def react_check(reaction, user):
                return user == ctx.author and (str(reaction.emoji) == '👍' or str(reaction.emoji) == '👎')

            reaction, user = await self.bot.wait_for("reaction_add", timeout=86400.0, check=react_check)
            if reaction == "👍":
                await ctx.send("Choose your team's name")
                m = await self.bot.wait_for("message", check=user_check)
                team_name = m.content
                tracker[str(counter)]["Teams"][str(ctx.author.id)] = {"name": team_name, "wins": 0, "losses": 0,
                                                                      "team": team}
            else:
                tracker[str(counter)]["Teams"][str(ctx.author.id)]["team"] = team
            save(tracker)
        await ctx.send("Team has been set!")

    @team.command(name="view")
    async def team_view_cmd(self, ctx, *, teamname=None):
        team_info = None
        tracker = reload_tracker()
        if teamname is not None:
            if teamname.startswith("<@!"):
                teamname = teamname.replace("<@!", "").replace(">", "").strip()
                team_info = tracker[str(counter)]["Teams"][teamname]  # Or rather ID here
            else:
                teams = tracker[str(counter)]["Teams"]
                for key, value in teams.items():
                    if teamname.lower() in value["name"].lower():
                        team_info = teams[key]
                if team_info is None:
                    await ctx.send("Could not find a team by that name.")
                    return
        else:
            team_info = tracker[str(counter)]["Teams"][str(ctx.author.id)]
        embed = discord.Embed(title=team_info["name"], colour=COLOR)
        batting_power = team_info["batting_power"]
        fielding_power = 0
        for key, value in team_info["team"].items():
            card = get_card(value["gacha"], pool)
            embed.add_field(name=key.title(), value="#" + str(card.card_id) + " | " + card.name)
            fielding_power += int(value["power"])
        embed.set_footer(text=f"Batting Power: {str(batting_power)}     Fielding Power: {fielding_power}")
        sent = await ctx.send(embed=embed)
        await sent.add_reaction("📊")

        def check(reaction, user):
            return user == ctx.author and str(reaction.emoji) == '📊'

        await self.bot.wait_for("reaction_add", check=check)
        embed = discord.Embed(title=team_info["name"], colour=COLOR)
        for key, value in team_info["team"].items():
            card = get_card(value["gacha"], pool)
            stat_msg_dict = {
                "height": f'Height: {card.stats["height"]}',
                "intelligence": f'Intelligence: {card.stats["intelligence"]}',
                "speed": f'Speed: {card.stats["speed"]}',
                "fortitude": f'Fortitude: {card.stats["fortitude"]}',
                "power": f'Power: {card.stats["power"]}',
                "total": f'Total: {card.stats["total"]}'
            }
            stat_dist = {
                "pitcher": ["height", "speed", "power"],
                "catcher": ["intelligence", "fortitude", "power"],
                "first base": ["height", "fortitude", "power"],
                "second base": ["speed", "intelligence", "power"],
                "third base": ["speed", "fortitude", "power"],
                "shortstop": ["height", "intelligence", "power"],
                "left outfield": ["power"],
                "center outfield": ["power"],
                "right outfield": ["power"],
                "coach": ["intelligence", "intelligence", "power"]
            }
            stat_msg = ""
            last_stat = ""
            try:
                for stat in stat_dist[key]:
                    if stat == last_stat:
                        break
                    stat_msg += stat_msg_dict[stat] + " "
                    last_stat = stat
            except TypeError:
                stat_msg = ""
            embed.add_field(name=key.title(), value="#" + str(card.card_id) + " | " + card.name + "\n" + stat_msg,
                            inline=False)
        await sent.delete()
        await ctx.send(embed=embed)

    @team.command(name="random")
    # @commands.check(preseason_check)
    async def team_random_cmd(self, ctx):
        msg = await ctx.send("Are you sure you want to randomize your team?")
        batting_power = 0
        await msg.add_reaction("👍")
        await msg.add_reaction("👎")

        def user_check(m):
            return m.author == ctx.author

        def check(reaction, user):
            return user == ctx.author and (str(reaction.emoji) == '👍' or str(reaction.emoji) == '👎')

        try:
            reaction, user = await self.bot.wait_for("reaction_add", timeout=86400.0, check=check)
        except asyncio.TimeoutError:
            pass
        if reaction.emoji == "👍":
            await ctx.send("Team randomized.")
            pool = get_gachas(ctx.author, GACHA_DATA)
            team = deepcopy(empty_team)
            choices = random.choices(pool, k=10)
            for key, gacha in zip(team.keys(), choices):
                team[key]["gacha"] = gacha.card_id
                team[key]["power"] = calculate_power(gacha, key)
                batting_power += gacha.stats["power"]
            tracker[str(counter)]["Teams"][str(ctx.author.id)] = {"name": f"{ctx.author.name}'s Team", "wins": 0,
                                                                  "losses": 0, "batting_power": batting_power,
                                                                  "team": team}
            save(tracker)
        else:
            await ctx.send("Team has not been randomized. Aborting...")

    @team.command(name="edit")
    # @commands.check(preseason_check)
    async def team_edit_cmd(self, ctx, role, gacha):
        gachas = get_gachas(ctx.author, GACHA_DATA)
        if role.lower() in empty_team.keys():
            for inv_gacha in gachas:
                if gacha.lower() in inv_gacha.name.lower():
                    # This is so ugly and long I can't even. V tired so ill look at this again tomorrow
                    tracker[str(counter)]["Teams"][str(ctx.author.id)]["team"][role.lower()] = inv_gacha.card_id
                    save(tracker)
                    await ctx.send(f"Set {inv_gacha.name} as your {role}")
                elif inv_gacha == gachas[-1]:
                    ctx.send("Could not find that gacha in your inventory. Please try again.")
                    return
        else:
            await ctx.send("Could not find that role. Please try again")
            return

    @team.command(name="reset")
    # @commands.check(preseason_check)
    async def team_reset_cmd(self, ctx):
        msg = await ctx.send("This will reset your baseball team. Are you sure?")
        await msg.add_reaction("👍")
        await msg.add_reaction("👎")

        def check(reaction, user):
            return user == ctx.author and (str(reaction.emoji) == '👍' or str(reaction.emoji) == '👎')

        try:
            reaction, user = await self.bot.wait_for("reaction_add", timeout=86400.0, check=check)
        except asyncio.TimeoutError:
            pass
        if reaction.emoji == "👍":
            await ctx.send("Team reset.")
            tracker[str(counter)]["Teams"][str(ctx.author.id)] = empty_team
            save(tracker)
        else:
            await ctx.send("Team has not been reset. Aborting...")


def setup(bot):
    bot.add_cog(Baseball(bot))
